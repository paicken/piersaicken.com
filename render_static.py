#! /usr/bun/python3

import argparse
import json
import markdown
import os
import pathlib
import re
import yaml

from collections import OrderedDict
from datetime import datetime
from jinja2 import Environment, FileSystemLoader

from config import Config


class StaticFileRender:
    def __init__(
        self,
        in_dir: str,
        out_dir: str,
        quiet_update: bool,
        force_update: bool,
    ) -> None:
        self.config: Config = Config()
        self.in_dir: str = in_dir
        self.index_md_exists: bool = False
        self.base_out_dir: str = out_dir
        self.update_index: bool = False
        self.force_update: bool = force_update
        self.quiet_update: bool = quiet_update

        self.in_files: list[str] = []

    def _get_files_to_render(self) -> list:
        files_in_path = self._get_all_files_in_path()
        if self.config.BLOG_INDEX_FILE in self.in_files:
            index_filename = f"index/{self.config.BLOG_INDEX_FILE}"
        else:
            index_filename = None
        if not self.in_files:  # Get all files in html and md directories
            self.in_files = files_in_path
        else:
            self.in_files = [fn for fn in self.in_files if fn in files_in_path]

        if index_filename:
            self.in_files.append(index_filename)

        if not self.in_files:
            raise FileNotFoundError(f"No provided filenames in input dir")

        file_paths = [f"{self.in_dir}/{filename}" for filename in self.in_files]
        self.files_to_render = file_paths

    def _get_all_files_in_path(self):
        entities_in_path = os.listdir(self.in_dir)
        files_in_path = []

        for entity in entities_in_path:
            entity_path = os.path.join(self.in_dir, entity)
            if os.path.isfile(entity_path):
                files_in_path.append(entity)
            elif entity == "index" and os.path.isdir(entity_path):
                self._update_index = True
                if os.path.isfile(f"{self.in_dir}/index/index.md"):
                    self.index_md_exists = True

        return files_in_path

    def _render_and_save_files(self):
        for file_path in self.files_to_render:
            render_config = self._render_and_save_file(file_path)
            if render_config["indexable"] == True:
                self._update_index_file(
                    render_config["output_filename"],
                    render_config["title"],
                    render_config["create_date"],
                )

    def _render_and_save_file(self, file_path: str):
        front_matter, md_content = self._read_and_parse_file(file_path)
        front_matter = self._update_front_matter_dict(front_matter)
        self._update_file_front_matter(file_path, front_matter, md_content)
        render_config = self._generate_render_config(front_matter)
        html_body = self._render_html_body(md_content)
        html_head = self._render_html_head(render_config)
        html_foot = self._render_html_foot(render_config)
        output_dir = self._determine_final_output_dir(render_config)
        self._create_output_dir(output_dir)
        output_filename = self._determine_output_filename(file_path, output_dir)
        render_config["output_filename"] = output_filename
        self._write_html_file(output_filename, html_head, html_body, html_foot)
        print(f"Rendered and saved {output_filename}")
        return render_config

    def _read_and_parse_file(self, input_file_path: str) -> tuple[dict, str]:
        with open(input_file_path, "r", encoding="utf-8") as input_file:
            input_file_content = input_file.read()
            return self._parse_raw_file(input_file_content)

    def _parse_raw_file(self, md_content: str) -> tuple[dict, str]:
        pattern = re.compile(r"^---\s*\n(.*?)\n---\s*\n(.*)", re.DOTALL)
        match = pattern.match(md_content)

        if match:
            front_matter_raw = match.group(1)
            md_content = match.group(2)
            front_matter = yaml.safe_load(front_matter_raw)
        else:
            front_matter = {}

        return front_matter, md_content

    def _update_front_matter_dict(self, front_matter: dict) -> dict:
        if "created_at" not in front_matter or "create_date" not in front_matter:
            create_datetime = datetime.now().strftime("%Y-%m-%d %H:%M")
            front_matter["created_at"] = f"{str(create_datetime)}"
            front_matter["create_date"] = f"{str(create_datetime.split()[0])}"
            # self._update_front_matter(input_file_path, front_matter, input_file_content)
        elif self.edit_update_time:
            front_matter[
                "updated_at"
            ] = f"{str(datetime.now().strftime('%Y-%m-%d %H:%M'))}"

        if not "show_date" in front_matter:
            front_matter["show_date"] = True
        if not "indexable" in front_matter:
            front_matter["indexable"] = True

        return front_matter

    def _update_file_front_matter(
        self, md_file_path: str, front_matter: dict, md_content: str
    ):
        front_matter_raw = yaml.dump(front_matter, default_flow_style=False)
        updated_md_content = f"---\n{front_matter_raw}---\n{md_content}"

        with open(md_file_path, "w", encoding="utf-8") as md_file:
            md_file.write(updated_md_content)

    def _generate_render_config(self, front_matter: dict) -> dict:
        render_conf = front_matter.copy()
        render_conf["protocol"] = self.config.PROTOCOL
        render_conf["server_name"] = self.config.SERVER_NAME
        render_conf["current_year"] = datetime.now().strftime("%Y")
        return render_conf

    def _render_html_body(self, md_content: str) -> str:
        html_content = markdown.markdown(
            md_content,
            extensions=["fenced_code", "pymdownx.tilde", "pymdownx.magiclink"],
        )
        return html_content

    def _render_html_head(self, render_config) -> str:
        html_head = self._render_template_from_file(
            f"{self.config.TEMPLATES_DIR}/", "page_head.html", render_config
        )
        return html_head

    def _render_html_foot(self, render_config) -> str:
        html_foot = self._render_template_from_file(
            f"{self.config.TEMPLATES_DIR}/", "page_foot.html", render_config
        )
        return html_foot

    def _render_template_from_file(
        self, template_path: str, template_file: str, render_config: dict
    ) -> str:
        env = Environment(loader=FileSystemLoader(template_path))
        template = env.get_template(template_file)
        return template.render(render_config)

    def _determine_final_output_dir(self, render_config: dict) -> str:
        if not render_config["show_date"]:
            return self.base_out_dir
        cdate = render_config["create_date"].split("-")
        return f"{self.base_out_dir}/{cdate[0]}/{cdate[1]}/{cdate[2]}"

    def _create_output_dir(self, output_dir: str):
        pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)

    def _determine_output_filename(self, in_file_path: str, output_dir: str):
        file_name = in_file_path.split("/")[-1]
        base_name = file_name.split(".")[0]
        return f"{output_dir}/{base_name}.html"

    def _write_html_file(self, out_file_name: str, head: str, body: str, foot: str):
        with open(out_file_name, "w", encoding="utf-8") as html_file:
            html_file.write(head)
            html_file.write(body)
            html_file.write(foot)

    def _update_index_file(self, output_filename: str, title: str, create_date: str):
        index_file_dir = f"{self.in_dir}/index"
        index_json_file = f"{index_file_dir}/index.json"
        index_links_file = f"{index_file_dir}/index_links.txt"
        index_md_file = f"{index_file_dir}/index.md"
        link_string = self._prepare_link_string(create_date, title, output_filename)
        json_updated = self._update_index_json_file(
            output_filename, title, create_date, index_json_file
        )
        if json_updated:
            self._prepend_link_to_links_txt(link_string, index_links_file)
            self._update_index_md_file(link_string, index_md_file)
        self._render_and_save_file(index_md_file)
    
    def _strip_base_from_output_filename(self, output_filename):
        relative_link = output_filename.replace(f"{self.base_out_dir}", "")
        try:
            relative_link = relative_link[1:] if relative_link[0] == "/" else relative_link
        except IndexError:
            pass
        return relative_link

    def _prepare_link_string(self, create_date: str, title: str, output_filename: str):
        relative_link = self._strip_base_from_output_filename(output_filename)
        return f"* {create_date} - [{title}]({relative_link})"

    def _prepend_link_to_links_txt(self, string_to_insert: str, index_links_file: str):
        with open(index_links_file, "r") as file:
            lines = file.readlines()
        lines.insert(0, string_to_insert + "\n")
        with open(index_links_file, "w") as file:
            file.writelines(lines)

    def _update_index_json_file(
        self, output_filename: str, title: str, create_date: str, index_json_file: str
    ):
        relative_link = self._strip_base_from_output_filename(output_filename)
        index_dict = self._load_index_json_file(index_json_file)
        if relative_link in index_dict:
            return False
        index_dict[relative_link] = {"title": title, "create_date": create_date}
        with open(index_json_file, "w") as out_file:
            json.dump(index_dict, out_file, indent=2, sort_keys=True)

        return True

    def _update_index_md_file(self, string_to_insert: str, index_md_file: str):
        with open(index_md_file, "r") as file:
            lines = file.readlines()
        for i, line in enumerate(lines):
            if self.config.FINAL_FIXED_INDEX_MD_LINE in line:
                lines.insert(i + 2, string_to_insert + "\n")
                break
        with open(index_md_file, "w") as file:
            file.writelines(lines)

    def run_render(self, in_files: list[str] = []):
        self.in_files = in_files
        self.edit_update_time: bool = self.force_update or not (
            self.quiet_update or not in_files
        )
        self._get_files_to_render()
        self._render_and_save_files()

    def refresh_and_remove(self):
        # check and create index dict file
        index_file_dir = f"{self.in_dir}/index"
        index_json_file = f"{index_file_dir}/index.json"
        index_links_file = f"{index_file_dir}/index_links.txt"
        self._check_index_dir_and_file_exist(
            index_file_dir, index_json_file, index_links_file
        )
        index_dict = self._load_index_json_file(index_json_file)
        existing_files_dict = self._files_in_dir_dict()
        expected_files_dict = self._sync_index_and_dir(
            index_dict, existing_files_dict, index_json_file
        )
        self._refresh_links_file(index_links_file, expected_files_dict)
        self._refresh_index_md(index_file_dir, index_links_file)
        if expected_files_dict:
            self.run_render([self.config.BLOG_INDEX_FILE])

        print("index updated")
        pass

    def _check_index_dir_and_file_exist(
        self, index_file_dir: str, index_json_file: str, index_links_file: str
    ):
        if not os.path.isdir(index_file_dir):
            pathlib.Path(index_file_dir).mkdir()

        if os.path.isfile(index_json_file):
            os.remove(index_json_file)

        if os.path.isfile(index_links_file):
            os.remove(index_links_file)

        file_list = self._build_initial_index_file()
        with open(index_json_file, "w") as out_file:
            json.dump(file_list, out_file, indent=2, sort_keys=True)

    def _build_initial_index_file(self) -> dict:
        expected_files = {}
        self._get_files_to_render()
        for file in self.files_to_render:
            filename = file.split("/")[-1].replace(".md", ".html")
            front_matter, _ = self._read_and_parse_file(file)
            if front_matter.get("indexable", True) == False:
                continue
            if front_matter.get("show_date", True) == False:
                file_path = filename
                create_date = "NA"
            else:
                create_date = front_matter["create_date"]
                cdate = create_date.split("-")
                file_path = f"{cdate[0]}/{cdate[1]}/{cdate[2]}/{filename}"
            expected_files[file_path] = {
                "title": front_matter["title"],
                "create_date": create_date,
            }
        if self.index_md_exists:
            expected_files["index/index.md"] = {"title": "Index", "create_date": None}
        return expected_files

    def _load_index_json_file(self, index_json_file: str) -> dict:
        with open(index_json_file, "r") as file:
            index_dict = json.load(file)
        return index_dict

    def _files_in_dir_dict(self) -> dict:
        files_dict = {}
        for root, dirs, files in os.walk(self.base_out_dir):
            for file in files:
                if file.split(".")[-1] != "html":
                    continue
                root = root.replace(f"{self.base_out_dir}", "")
                try:
                    root = root[1:] if root[0] == "/" else root
                except IndexError:
                    pass
                file_path = os.path.join(root, file)
                files_dict[file_path] = True
        return files_dict

    def _sync_index_and_dir(
        self, index_dict: dict, dir_dict: dict, index_json_file: str
    ) -> dict:
        index_dict_keys = set(index_dict.keys())
        dir_dict_keys = set(dir_dict.keys())
        common_keys = index_dict_keys.intersection(dir_dict_keys)
        common_dict = {key: index_dict[key] for key in common_keys}

        for filename in dir_dict_keys - index_dict_keys:
            try:
                os.remove(f"{self.base_out_dir}/{filename}")
                print(f"Removed file {filename}")
            except OSError as e:
                print(f"Error removing file {filename}: {e}")

        with open(index_json_file, "w") as out_file:
            json.dump(common_dict, out_file, indent=2, sort_keys=True)

        return common_dict

    def _refresh_links_file(self, index_links_file: str, index_dict: dict) -> str:
        with open(index_links_file, "w") as file:
            for key in index_dict:
                file.write(
                    f"* {index_dict[key]['create_date']} - [{index_dict[key]['title']}]({key})\n"
                )

    def _refresh_index_md(self, index_file_dir: str, index_links_file: str):
        links = self._get_links_from_index_links_txt(index_links_file)
        index_md_file = f"{index_file_dir}/{self.config.BLOG_INDEX_FILE}"
        if os.path.exists(index_md_file):
            self._strip_links_from_index_md_file(index_md_file)
        else:
            self._initialse_index_md_file_from_template(index_md_file)

        with open(index_md_file, "a") as file:
            for link in links:
                file.write(link)

    def _get_links_from_index_links_txt(self, index_links_file: str) -> list[str]:
        with open(index_links_file, "r") as file:
            links = file.readlines()
        return links

    def _strip_links_from_index_md_file(self, index_md_file: str):
        with open(index_md_file, "r") as file:
            md_lines = file.readlines()

        for i, md_line in enumerate(md_lines):
            if self.config.FINAL_FIXED_INDEX_MD_LINE in md_line:
                md_lines = md_lines[: i + 2]
                break
        with open(index_md_file, "w") as file:
            file.writelines(md_lines)

    def _initialse_index_md_file_from_template(self, index_md_file: str):
        with open(
            f"{self.config.TEMPLATES_DIR}/{self.config.INDEX_TEMPLATE}", "r"
        ) as file:
            template_head = file.read()
        with open(index_md_file, "w") as file:
            file.write(template_head)


def load_settings_from_file(settings_file: str) -> dict:
    with open(settings_file, "r") as file:
        settings_dict = json.load(file)
    return settings_dict


def main():
    parser = argparse.ArgumentParser(description="Render static html files")
    parser.add_argument(
        "filenames", metavar="filename", type=str, nargs="*", help="Files to convert"
    )
    parser.add_argument(
        "-f",
        "--force-update",
        action="store_true",
        help="Force all files to update date info - overrides '--quiet-update'",
    )
    parser.add_argument(
        "-i",
        "--input-dir",
        default=Config.DEFAULT_INPUT_DIR,
        help=f"Input directory (default: {Config.DEFAULT_INPUT_DIR})",
    )
    parser.add_argument(
        "-o",
        "--output-dir",
        default=Config.DEFAULT_OUTPUT_DIR,
        help=f"Output directory (default: {Config.DEFAULT_OUTPUT_DIR})",
    )
    parser.add_argument(
        "-q",
        "--quiet-update",
        action="store_true",
        help="Do not mark files updated (for refreshing head/foot html)",
    )
    parser.add_argument(
        "-r",
        "--refresh-and-remove",
        action="store_true",
        help="Regenerate index for directory and remove deleted files",
    )
    parser.add_argument(
        "-R",
        "--refresh-and-remove-only",
        action="store_true",
        help="Regenerate index for directory and remove deleted files then exit",
    )
    parser.add_argument(
        "-s",
        "--settings-file",
        default=None,
        help="Optional JSON file to load settings from",
    )

    args = parser.parse_args()

    settings_file_dict = load_settings_from_file(args.settings_file) if args.settings_file else {}
    input_dir = settings_file_dict.get("input_dir", args.input_dir)
    output_dir = settings_file_dict.get("output_dir", args.output_dir)
    quiet_update = settings_file_dict.get("quiet_update", args.quiet_update)
    force_update = settings_file_dict.get("force_update", args.force_update)
    filenames = settings_file_dict.get("filenames", args.filenames)
    refresh_and_remove = settings_file_dict.get("refresh_and_remove", args.refresh_and_remove)
    refresh_and_remove_only = settings_file_dict.get("refresh_and_remove_only", args.refresh_and_remove_only)

    renderer = StaticFileRender(
        input_dir,
        output_dir,
        quiet_update,
        force_update,
    )

    if refresh_and_remove or refresh_and_remove_only:
        renderer.refresh_and_remove()
        if refresh_and_remove_only:
            renderer.in_files = ["index.md"]
            exit()
        else:
            renderer.in_files = []
            renderer.edit_update_time = force_update

    renderer.run_render(filenames)


if __name__ == "__main__":
    main()
