import json
import os
from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, ".env"))


class Config:
    SERVER_NAME = os.environ.get("SERVER_NAME", "localhost:5000")
    PROTOCOL = os.environ.get("PROTOCOL", "http")
    TEMPLATES_DIR = os.environ.get("TEMPLATES_DIR", "templates")
    BLOG_INDEX_FILE = os.environ.get("BLOG_INDEX_FILE", "index.md")
    RAW_FILE_DIRS = json.loads(
        os.environ.get("RAW_FILE_DIRS", '["raw/blog", "raw/www"]')
    )
    DEFAULT_INPUT_DIR = os.environ.get("DEFAULT_INPUT_DIR", "raw/blog")
    DEFAULT_OUTPUT_DIR = os.environ.get("DEFAULT_OUTPUT_DIR", "html/blog")
    INDEX_TEMPLATE = os.environ.get("INDEX_TEMPLATE", "index_template.md")
    FINAL_FIXED_INDEX_MD_LINE = os.environ.get(
        "FINAL_FIXED_INDEX_MD_LINE", "Latest posts:"
    )
