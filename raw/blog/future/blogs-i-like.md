---
title: Blogs I Like
subtitle: A non-exhaustive list of blogs I find interesting/useful
---
* [https://blog.jim-nielsen.com/](https://blog.jim-nielsen.com/)
* [https://sebastiandedeyne.com/](https://sebastiandedeyne.com/)
* [https://thunk.dev/blog](https://thunk.dev/blog)