---
create_date: '2023-12-30'
created_at: 2023-12-30 18:57
indexable: true
show_date: true
subtitle: Features I'd like to implement, bugs I'd like to fix, or just general improvements
  I'd like to make to this website at some point in the future
title: Website Improvements
---
This is essentially my site's equivalent of the back of an envelope used to take scrappy notes on, in this specificaly a TODO list of things I want to fix or improve on my website. I'll update it as and when something pops into my head or I get round to completing an item.

The list:

* A footer that doesn't hang off screen on an empty page
* Blog tags
* GitLab CI to deploy to server
* Deleting removed posts from blog index
* Not readding existing posts to blog index
* ~~Better dates in URL (e.g. blog.piersaicken.com/2023/12/30/website-improvements.html)~~
* Ansible server set up 
