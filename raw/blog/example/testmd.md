---
metatags:
- content: This is a test page
  name: description
- content: test page
  name: keywords
subtitle: An article to test rendering of various components on a piersaicken.com
  blog post
title: Testing the conversion of markdown to html
---
Donec sed dolor rhoncus, consequat est eget, efficitur dui. Vestibulum vel diam est. Praesent sed libero ut orci lobortis molestie. Pellentesque non tellus in arcu eleifend vulputate. Vivamus efficitur maximus sapien, at ullamcorper tellus blandit ut. Aliquam erat volutpat. Vivamus quis enim semper, vulputate augue vitae, bibendum tellus. Maecenas maximus luctus justo nec ultricies. Nunc vitae mattis ipsum, sollicitudin ornare neque.

Proin rhoncus sit amet enim sollicitudin commodo. Nunc tempus semper felis, eget volutpat magna pharetra ut. Suspendisse ut porttitor turpis. Aenean nec ipsum ac justo placerat tristique sit amet nec nisi. Nulla facilisi. Vestibulum scelerisque faucibus elementum. Nullam quis sodales magna. Curabitur convallis velit dolor, ut viverra nisi faucibus non. Suspendisse quis felis diam.

subtitle: An article to test rendering of various components on a piersaicken.com blog post

## This is an h2 header

This is a paragraph with a [link](https://www.example.com)

* This is a list
* This is another list item

### H3 header

1. Num list part 1
2. Num list part 2
3. Num list part 3

Trying to add an image:

![Alt text](/static/images/pixabay-html.jpg "Optional title")

```
Test code section
```

An early `<code>` css:

```
code {
    background-color: #f4f4f4;
    color: #d63384;
    font-family: 'Courier New', Courier, monospace;
    padding: 2px 4px;
    border-radius: 4px;
    border: 1px solid #ccc;
    font-size: 0.9em;
}
```

Another attempt at a code block using explicit `<pre>` tags:

<pre>
Hello world
</pre>