---
create_date: '2023-12-30'
created_at: 2023-12-30 11:03
indexable: true
show_date: true
subtitle: Projects, Exploration, and Technical Musings - My personal workshop journal
title: About This Site
---
Welcome to PiersAicken.com, my personal digital journal and notepad. Here, I plan to document any technical challenges I face, (hopefully) overcome, and that I find interesting, as well as personal thoughts or musings I may have. While this site aims primarily to provide myself with future reference, I'm delighted if anyone else should find it useful or interesting.

This site is also intended to be a personal sandpit to enable me to enable me to experiment and try out new things. The core website and this blog are currently static sites comprising solely of static HTML and CSS which I intend to stick to, at least for the moment, but should I ever get round to learning [React](https://react.dev/), a refresh of this website could be in order. Equally, I might extend the site in other ways through various subdomains.

Everything about this site is stored in a git repository and it makes little difference to me where I decide to host it. Ultimately my site and set-up acheive what I wanted - a space I'm in charge of and can play in, free of the control, whims, and unpredictability of some other organisation.

## Implementation

`piersaicken.com` was created with a home-rolled static site generator. It's a fairly straight-forward python script that takes markdown files as input, converts those to HTML using [Python-Markdown](https://python-markdown.github.io/), combines that with a couple of rendered jinja templates, and spits out a `.html` file for me to serve with Nginx.

#### Why home-rolled?

The original intended purpose of this website was to allow me to produce posts, not to get caught up in the technical implementation so my initial plan was to use a mature static site generator, likely either [Hugo](https://gohugo.io/) or [Jekyll](https://jekyllrb.com/). However, as straight forward as these are to get started with, they're considerably more complex than I need or want. A simple script to produce some HTML struck me as being ample for my use case. Additionally, because I control every line of code related to my site, it gives me the flexibility to edit or extend it how I desire, without having to learn a new ecosystem. Finally, it's more fun - while the original sole purpose of my personal website was to write, I enjoy tinkering and being able to point at my website and say "I made that" gives me far more satisfaction than spinning up a ready-made solution, despite the end result being considerably cruder and less impressive.

#### Why python?

Speed of implementation - having worked professionally with it since graduating in 2015 (and using it for my degree's final year project), I'm comfortable with it and could focus on producing the generator script rather than navigating a less familiar laguage. It's not the most efficient, and I know there are definite improvements I could make to the script, but I wanted to get going quickly, and due to the small site, with likely infrequent updates, it doesn't need to be extremely efficient during execution.

### Deploying a static site with subdomains to Nginx on an Ubuntu AWS EC2 instance

This site is deployed on an AWS EC2 instance running Ubuntu with security groups allowing SSH, HTTP and HTTPS. I have installed Nginx and opened HTTP and HTTPS ports on the firewall:

```
$ sudo ufw allow 'Nginx Full'
$ sudo ufw enable
$ sudo systemctl start nginx
$ sudo systemctl enable nginx
```

Next, in order to access my website using my domain rather than a raw IP address, I've added records to the site's DNS in Namecheap (my preferred registrar since the [SOPA contoversy](https://www.namecheap.com/blog/we-say-no-to-sopa/) a number of years back):

![DNS Records](/static/images/piersaickencom-dns-records.png "DNS Records")

A few minutes later, confirm the DNS has worked and the server is accessible:

![Initial URL Check](/static/images/initial-url-check.png "Initial URL Check")


In the not too distant future, I plan to have this site updated automatically using GitLab CI so when a post is added/edited/deleted, and this is committed and pushed to GitLab, the CI will automatically generate the HTML files and push them to this repository. To achieve this, I've created a bare git repository at `/var/www/piersaicken.com` so the site repository can be pushed to it, eventually automatically with GitLab CI, but currently manually from my local laptop.

```
$ sudo mkdir -p /var/www/piersaicken.com
$ cd /var/www/piersaicken.com
$ git init --bare
$ sudo bash
# cat << 'EOF' > hooks/post-receive
#!/bin/sh
GIT_WORK_TREE=/var/www/piersaicken.com git checkout -f
EOF
# exit
$ sudo chmod +x hooks/post-receive
$ sudo chown -R $USER:$USER /var/www/piersaicken.com/
$ sudo chmod -R 755 /var/www

```

Set up the configuration for the subdomains. Note allowing CORS my blog and any future subdomains can access fonts served at the main domain.

```
$ sudo bash
# cat << 'EOF' > /etc/nginx/sites-available/www.piersaicken.com
server {
    listen 80;
    listen [::]:80;

    root /var/www/piersaicken.com/html/www;
    index home.html;

    server_name www.piersaicken.com piersaicken.com;

    location / {
        try_files $uri $uri/ =404;
    }

    location ~* \.(eot|ttf|woff|woff2|otf)$ {
        if ($http_origin ~* (https?://(.+\.)?piersaicken\.com)) {
            add_header "Access-Control-Allow-Origin" "$http_origin";
            add_header Access-Control-Allow-Methods "GET, OPTIONS";
        }
    }

}
EOF

# cat << 'EOF' > /etc/nginx/sites-available/blog.piersaicken.com
server {
    listen 80;
    listen [::]:80;

    root /var/www/piersaicken.com/html/blog;
    index index.html;

    server_name blog.piersaicken.com;

    location / {
        try_files $uri $uri/ =404;
    }
}
EOF

# exit
```

Then link the configurations to the sites-enabled directory:

```
$ sudo ln -s /etc/nginx/sites-available/blog.piersaicken.com /etc/nginx/sites-enabled/
$ sudo ln -s /etc/nginx/sites-available/www.piersaicken.com /etc/nginx/sites-enabled/
```

Test the new Nginx configuration, to be sure, to be sure!

```
$ sudo nginx -t
```

Restart Nginx:

```
$ sudo systemctl restart nginx
```

At this point, setting up HTTPS is painless:

```
$ sudo apt update
$ sudo apt install certbot python3-certbot-nginx
$ sudo certbot --nginx
```

After running this last commmand, it's simply a case of through flying through the interactive wizard and voilà - I now have a running website with multiple subdomains and HTTPS enabled!