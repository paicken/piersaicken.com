---
create_date: '2024-01-04'
created_at: 2024-01-04 03:09
indexable: false
show_date: false
title: Hello, World
updated_at: 2024-01-10 20:05
---
### I'm Piers Aicken

* **Platform Engineer** - specialising in Linux, Python, Cloud-computing and automation
* **Passionate about Technology** - Continually exploring and adopting innovative solutions
* **Skills at a glance** - Linux | Python | Cloud | IaC | Bash | CI/CD
* **More About Me** - Become familiar with me and my site in my [About](about.html) section
* **My Thoughts** - Read my thoughts and ramblings in my [Blog](https://blog.piersaicken.com)
* **Everything Else** - See my varied projects and anything not categorised above in the  [Misc](misc.html) section
* **Contact Me** - My socials / contact details are on my [contact page](contact.html)