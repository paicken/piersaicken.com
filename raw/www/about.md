---
create_date: '2024-01-04'
created_at: 2024-01-04 03:09
indexable: false
show_date: false
sub-title: A Tinkerer's Tales in tech
title: About This Site
updated_at: 2024-01-10 03:18
---
When my father arrived home one day in 1996 with a giant mysterious box in his arms, little did he suspect he was about to set me on a path that would shape the rest of my life. For in that box was a brand new Windows 95 PC (with its healthy 100MHz Pentium processor and 16Mb of RAM) that spawned a fascination for computers and technology that is as strong in me today as it was in my 5-year old self all those years ago.

Technology excites me as much as Christmas excites a child, intrigues me as well as any murder mystery, and provides me with immense satisfaction when I'm able to point at something I've created and say "I made that". Technology has always been my passion and I feel very lucky it's also my profession.

This website is my tiny corner in the vastness of the internet; it's my notebook to document my forays into technology, my journal to record my thoughts and practice my writing, my laboratory to explore and experiment with tech, and, most importantly, my own dominion - a place I have full control over, free from the control of any individual large tech organisation or being at their mercy. It's been created primarily to scratch an itch and to allow me to centralise my various project notes and thoughts, but should you find it interesting or informative I am humbled to be of assistance.

By profession, I'm a Platform Engineer, my daily bread-and-butter consisting of Linux, Bash, Python, and Cloud Computing, but my my experience and interests are considerably broader; networking, virtualisation, and microelectronics a few interests that spring to mind. However, despite the saying that appearances can be deceiving, I doubt the appearance of this website design will deceive anyone into believing front-end design is one of my strengths!

When I occasionally escape the digital world, I'm most likely on a badminton court somewhere – enthusiastically, if not expertly, swinging a racket. That or, admittedly less often than I'd like, lost in the realms of fantasy – be it through books, movies, or games.

If you're keen to chat about tech, or anything else, drop me a message using one of the links below!

Cheers,

Piers