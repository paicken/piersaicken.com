---
create_date: '2024-01-04'
created_at: 2024-01-04 03:09
indexable: false
show_date: false
title: Contact Me
updated_at: 2024-01-10 20:08
---
&nbsp;<div class="contact-container">

[![LinkedIn](/static/images/linkedin.png "LinkedIn") LinkedIn](https://www.linkedin.com/in/piers-aicken/)

[![GitHub](/static/images/github.png "GitHub") GitHub](https://github.com/paicken)

[![GitLab](/static/images/gitlab.png "GitLab") GitLab](https://gitlab.com/paicken)

[![Twitter](/static/images/twitter.png "Twitter") Twitter / X](#)

[![Email](/static/images/email.png "Email") Email](mailto:aickenpga@gmail.com)

</div>